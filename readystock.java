class readystock extends kue{
    private double jumlah;//membuat variable baru jumlah
    public readystock(String nama,int harga,int jumlah){
        super(nama, harga);// menggunakan super untuk memanggil variable yang sudah adsa di parent
        this.jumlah=jumlah;// this untuk menyimpan jumlah
    } 
    public double hitungHarga(){//method untuk menghitung harga
        return jumlah*2*super.getPrice();
    }
    public double Jumlah(){
        return jumlah;// mereturn ke jumlah
    }
    @Override
    public double Berat() {
       return 0.0;// mereturn 0 karena beratnya tidak ada
    }
}