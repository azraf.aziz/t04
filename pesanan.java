class pesanan extends kue{
    private double berat;//membuat variable untuk menyimpan berat
    public pesanan(String nama,int harga,int berat){
        super(nama, harga);//menggunakan super karena nama dan harga sudah dad di class parent
        this.berat=berat;//menyimpan berat dengan menggunakan this
    }
    public double hitungHarga(){//methid untuk menghitung harga
        return berat*super.getPrice();//menghitung harga
    }
    @Override
    public double Berat() {
      return berat;//mereturn nilai dari berat
    }
    @Override
    public double Jumlah() {
       return 0.0;//mereturn dari nilai jumlah
    }
}